import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public message = '';

  constructor(private authService: AuthService, private router: Router) {}

  login(email: string, password: string) {
    // todo proper form / validation

    this.authService
      .login({ strategy: 'local', email, password })
      .then(() => {
        this.router.navigate(['/']);
      })
      .catch(() => {
        this.message = 'Please verify your credentials';
      });
  }

  signup(email: string, password: string) {
    this.authService
      .signUp({ email, password })
      .then(() => (this.message = 'User created'))
      .catch(() => (this.message = "Couldn't create user"));
  }
}
