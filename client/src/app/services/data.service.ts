import { Injectable } from '@angular/core';
import { FeathersService } from './feathers.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private feathersService: FeathersService) {}

  messages$() {
    return this.feathersService
      .service('messages')
      .watch()
      .find({
        query: {
          $sort: { createdAt: -1 },
          $limit: 25
        }
      });
  }

  users$() {
    return this.feathersService.service('users').watch().find();
  }

  sendMessage(message: string) {
    if (message === '') return;

    this.feathersService.service('messages').create({ text: message });
  }
}
