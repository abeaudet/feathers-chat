import { Injectable } from '@angular/core';
import * as feathersAuthClient from '@feathersjs/authentication-client';
import * as feathers from '@feathersjs/feathers';
import feathersSocketIOClient from '@feathersjs/socketio-client';
import * as feathersRx from 'feathers-reactive';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class FeathersService {
  private _feathers = feathers();
  private _socket = io('http://localhost:3030');
  private _feathersAuthClient = feathersAuthClient.default;

  constructor() {
    this._feathers
      .configure(feathersSocketIOClient(this._socket)) // add socket.io plugin
      .configure(
        this._feathersAuthClient({
          storage: window.localStorage
        })
      )
      .configure(
        feathersRx({
          idField: '_id'
        })
      );
  }

  // expose services
  public service(name: string) {
    return this._feathers.service(name);
  }

  // expose authentication
  public authenticate(credentials?: any): Promise<any> {
    if (!credentials) return this._feathers.reAuthenticate();
    return this._feathers.authenticate(credentials);
  }

  // expose logout
  public async logout() {
    return await this._feathers.logout();
  }
}
