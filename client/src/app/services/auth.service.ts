import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FeathersService } from './feathers.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private feathersService: FeathersService,
    private router: Router
  ) {}

  public login(creds?: any): Promise<any> {
    return this.feathersService.authenticate(creds);
  }

  signUp(creds: any) {
    return this.feathersService.service('users').create(creds);
  }

  public async logout() {
    await this.feathersService.logout();
    this.router.navigate(['/']);
  }
}
